var respQueryDeviceTrackProto = {
    "options": {
        "java_package": "com.minigps.proto",
        "java_outer_classname": "RespQueryDeviceTrackRecordOut"
    },
    "nested": {
        "proto": {
            "nested": {
                "RespQueryDeviceTrackProto": {
                    "fields": {
                        "status": {
                            "type": "int32",
                            "id": 1
                        },
                        "cause": {
                            "type": "string",
                            "id": 2
                        },
                        "deviceid": {
                            "type": "string",
                            "id": 3
                        },
                        "records": {
                            "rule": "repeated",
                            "type": "RespQueryDeviceTrackRecordProto",
                            "id": 4
                        }
                    }
                },
                "RespQueryDeviceTrackRecordProto": {
                    "fields": {
                        "trackCount": {
                            "type": "int32",
                            "id": 1
                        },
                        "starttime": {
                            "type": "sint64",
                            "id": 2
                        },
                        "endtime": {
                            "type": "sint64",
                            "id": 3
                        },
                        "trackid": {
                            "type": "int32",
                            "id": 4
                        },
                        "arrivedtime": {
                            "type": "sint64",
                            "id": 5
                        },
                        "radius": {
                            "type": "int32",
                            "id": 6
                        },
                        "recorderspeed": {
                            "type": "double",
                            "id": 7
                        },
                        "altitude": {
                            "type": "double",
                            "id": 8
                        },
                        "course": {
                            "type": "int32",
                            "id": 9
                        },
                        "strstatus": {
                            "type": "string",
                            "id": 10
                        },
                        "strstatusen": {
                            "type": "string",
                            "id": 11
                        },
                        "gotsrc": {
                            "type": "string",
                            "id": 12
                        },
                        "rxlevel": {
                            "type": "int32",
                            "id": 13
                        },
                        "reportmode": {
                            "type": "sint32",
                            "id": 14
                        },
                        "gpsvalidnum": {
                            "type": "sint32",
                            "id": 15
                        },
                        "weight": {
                            "type": "sint64",
                            "id": 16
                        },
                        "reissue": {
                            "type": "sint32",
                            "id": 17
                        },
                        "address": {
                            "type": "string",
                            "id": 18
                        },
                        "voltage": {
                            "type": "sint32",
                            "id": 19
                        },
                        "oilrate": {
                            "type": "sint32",
                            "id": 20
                        },
                        "updatetime": {
                            "type": "sint64",
                            "id": 21
                        },
                        "status": {
                            "type": "sint64",
                            "id": 22
                        },
                        "speed": {
                            "type": "double",
                            "id": 23
                        },
                        "callat": {
                            "type": "double",
                            "id": 24
                        },
                        "callon": {
                            "type": "double",
                            "id": 25
                        },
                        "totaldistance": {
                            "type": "sint32",
                            "id": 26
                        },
                        "totalad": {
                            "type": "sint32",
                            "id": 27
                        },
                        "totalnotrunningad": {
                            "type": "sint32",
                            "id": 28
                        },
                        "ad0": {
                            "type": "sint32",
                            "id": 29
                        },
                        "ad1": {
                            "type": "sint32",
                            "id": 30
                        },
                        "ad2": {
                            "type": "sint32",
                            "id": 31
                        },
                        "ad3": {
                            "type": "sint32",
                            "id": 32
                        },
                        "srcad0": {
                            "type": "sint32",
                            "id": 33
                        },
                        "srcad1": {
                            "type": "sint32",
                            "id": 34
                        },
                        "srcad2": {
                            "type": "sint32",
                            "id": 35
                        },
                        "srcad3": {
                            "type": "sint32",
                            "id": 36
                        }
                    }
                }
            }
        }
    }
}